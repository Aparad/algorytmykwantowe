# -*- coding: utf-8 -*-
import math


class ComplexNumber:
    """ Liczba zespolona: a + bi.
        Obsługuje:
        - dodawanie
        - odejmowanie
        - mnożenie
        - dzielenie
        - moduł
        - sprzężenie
    """

    def __init__(self, real, imaginary):
        self.Re = real
        self.Im = imaginary

    def __str__(self):
        sign = "+" if self.Im >= 0 else ""
        return "{re} {sign}{im}i".format(sign=sign, re=self.Re, im=self.Im)

    def __add__(self, other):
        return ComplexNumber(self.Re + other.Re, self.Im + other.Im)

    def __sub__(self, other):
        return ComplexNumber(self.Re - other.Re, self.Im - other.Im)

    def __mul__(self, other):
        return ComplexNumber((self.Re * other.Re - self.Im * other.Im), (self.Re * other.Im + self.Im * other.Re))

    def __truediv__(self, other):
        return ComplexNumber((self.Re * other.Re + self.Im * other.Im) / ((other.Re * other.Re) + (other.Im * other.Im)), (self.Im * other.Re - self.Re * other.Im) / ((other.Re * other.Re) + (other.Im * other.Im)))

    def add(self, C):
        return ComplexNumber(self.Re + C.Re, self.Im + C.Im)

    def subtract(self, C):
        return ComplexNumber(self.Re - C.Re, self.Im - C.Im)

    def multiply(self, C):
        return ComplexNumber((self.Re * C.Re - self.Im * C.Im), (self.Re * C.Im + self.Im * C.Re))

    def divide(self, C):
        return ComplexNumber((self.Re * C.Re + self.Im * C.Im) / ((C.Re * C.Re) + (C.Im * C.Im)), (self.Im * C.Re - self.Re * C.Im) / ((C.Re * C.Re) + (C.Im * C.Im)))

    def divByScalar(self, scalar):
        return ComplexNumber((self.Re / scalar), (self.Im / scalar))

    def module(self):
        return math.sqrt((self.Re * self.Re) + (self.Im * self.Im))

    def conjugate(self):
        return ComplexNumber(self.Re, - self.Im)
