# -*- coding: utf-8 -*-
from matrix import Matrix
from complexnumber import ComplexNumber
from math import sqrt


Gate_X = Matrix(([ComplexNumber(0, 0), ComplexNumber(1, 0)], [ComplexNumber(1, 0), ComplexNumber(0, 0)]))
Gate_Y = Matrix(([ComplexNumber(0, 0), ComplexNumber(0, -1)], [ComplexNumber(0, 1), ComplexNumber(0, 0)]))
Gate_Z = Matrix(([ComplexNumber(1, 0), ComplexNumber(0, 0)], [ComplexNumber(0, 0), ComplexNumber(-1, 0)]))
Gate_H = Matrix(([ComplexNumber(1 / sqrt(2), 0), ComplexNumber(1 / sqrt(2), 0)], [ComplexNumber(1 / sqrt(2), 0), ComplexNumber(-1 / sqrt(2), 0)]))

Zero_Bit = Matrix(([ComplexNumber(1, 0)], [ComplexNumber(0, 0)]))
One_Bit = Matrix(([ComplexNumber(0, 0)], [ComplexNumber(1, 0)]))

M_Zero = Matrix(([ComplexNumber(1, 0)], [ComplexNumber(0, 0)], [ComplexNumber(0, 0)], [ComplexNumber(0, 0)]))
M_One = Matrix(([ComplexNumber(0, 0)], [ComplexNumber(0, 0)], [ComplexNumber(0, 0)], [ComplexNumber(1, 0)]))