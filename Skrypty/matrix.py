# -*- coding: utf-8 -*-
import numpy


class Matrix:
    def __init__(self, matrix):
        self.matrix = numpy.array(matrix)

    def __mul__(self, other):
        ret = Matrix(([]))
        ret.matrix = numpy.dot(self.matrix, other.matrix)
        return ret

    def __str__(self):
        ret = ""
        for i in range(self.matrix.shape[0]):
            ret += "["
            for j in range(self.matrix.shape[1]):
                ret += str(self.matrix[i][j]) + " "
            ret += "]\n"
        return ret

    def tensorDot(self, other):
        ret = Matrix(([]))
        ret.matrix = numpy.tensordot(self.matrix, other.matrix)
        return ret
