# -*- coding:utf-8 -*-
from complexnumber import ComplexNumber


class ComplexVector:
    """ Klasa wektora liczb zespolonych.
        Obsługuje:
        - dodawanie
        - odejmowanie
        - mnożenie przez skalar
        - iloczyn skalarny
    """
    def __init__(self):
        self.v = []

    def __str__(self):
        s = "["
        for v in self.v[:-1]:
            s += str(v) + ", "
        s += str(self.v[-1]) + "]"
        return s

    def addNewNumber(self, C):
        self.v.append(C)

    def add(self, vector):
        newVector = ComplexVector()
        for i in range(len(self.v)):
            temp = self.v[i].add(vector.v[i])
            newVector.v.append(ComplexNumber(temp.Re, temp.Im))
        return newVector

    def subtract(self, vector):
        newVector = ComplexVector()
        for i in range(len(self.v)):
            temp = self.v[i].subtract(vector.v[i])
            newVector.v.append(ComplexNumber(temp.Re, temp.Im))
        return newVector

    def multiplyBy(self, g):
        newVector = ComplexVector()
        if isinstance(g, int):
            for i in range(len(self.v)):
                newVector.v.append(ComplexNumber(self.v[i].Re * g, self.v[i].Im * g))
            return newVector
        elif isinstance(g, ComplexNumber):
            for i in range(len(self.v)):
                newVector.v.append(self.v[i].multiply(g))
            return newVector

    def dotProduct(self, vector):
        currentSum = ComplexNumber(0, 0)
        for i in range(len(self.v)):
            alphaconjugate = self.v[i].conjugate()
            vectormultiply = vector.v[i].multiply(alphaconjugate)
            currentSum = currentSum.add(vectormultiply)
        return currentSum
