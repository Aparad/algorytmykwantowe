#-*- coding: utf-8 -*-
from complexnumber import ComplexNumber
from complexvector import ComplexVector
from matrix import Matrix
from quantum import Gate_H, Gate_X, Gate_Y, Gate_Z, Zero_Bit, One_Bit, M_One, M_Zero
from qubit import Qubit
import math


if __name__ == "__main__":
    print("Liczby zespolone: \n")
    C1 = ComplexNumber(2, 3)
    C2 = ComplexNumber(2, 2)

    print("C1: {c}".format(c=C1))
    print("C2: {c}".format(c=C2))
    print("Dodawanie: ({c1}) + ({c2}) = {wynik}".format(c1=C1, c2=C2, wynik=C1.add(C2)))
    print("Odejmowanie: ({c1}) - ({c2}) = {wynik}".format(c1=C1, c2=C2, wynik=C1.subtract(C2)))
    print("Mnożenie: ({c1}) * ({c2}) = {wynik}".format(c1=C1, c2=C2, wynik=C1.multiply(C2)))
    print("Dzielenie: ({c1}) / ({c2}) = {wynik}".format(c1=C1, c2=C2, wynik=C1.divide(C2)))
    print("Moduł: ({c1}) = {wynik}".format(c1=C1, wynik=C1.module()))
    print("Sprzężenie: ({c1}) = {wynik}".format(c1=C1, wynik=C1.conjugate()))


    print("______________________\n")
    print("Wektory:\n")
    V1 = ComplexVector()
    #V1.addNewNumber(ComplexNumber(0, 0))
    #V1.addNewNumber(ComplexNumber(0, 1))
    V1.addNewNumber(ComplexNumber(1, 2))
    V1.addNewNumber(ComplexNumber(3, -4))

    V2 = ComplexVector()
    #V2.addNewNumber(ComplexNumber(0, 0))
    #V2.addNewNumber(ComplexNumber(0, -1))
    V2.addNewNumber(ComplexNumber(-1, -1))
    V2.addNewNumber(ComplexNumber(2, -1))

    gamma = ComplexNumber(1, 1)

    print("V1 = {v}".format(v=V1))
    print("V2 = {v}".format(v=V2))
    print("Gamma = {gamma}".format(gamma=gamma))
    print("Dodawanie: {wynik}".format(wynik=V1.add(V2)))
    print("Odejmowanie: {wynik}".format(wynik=V1.subtract(V2)))
    print("Mnożenie przez Gamma: {wynik}".format(wynik=V1.multiplyBy(gamma)))
    print("Iloczyn skalarny: {wynik}".format(wynik=V1.dotProduct(V2)))

    print("______________________\n")
    print("Bramki kwantowe:\n")
    M = Matrix(([ComplexNumber(1/math.sqrt(2), 0)], [ComplexNumber(1/math.sqrt(2), 0)]))
    print("M = %s" % M)

    print("M * X =")
    MX = Gate_X * M
    print(MX)

    print("M * Y =")
    MY = Gate_Y * M
    print(MY)

    print("M * Z =")
    MZ = Gate_Z * M
    print(MZ)

    print("M * H =")
    MH = Gate_H * M
    print(MH)

    print("______________________\n")
    print("Pomiary qubitu:\n")
    qubit = Qubit(ComplexNumber(1/math.sqrt(2), 0), ComplexNumber(1/math.sqrt(2), 0))
    print("Qubit:")
    print(qubit)

    print("P(0)= %s" % qubit.p_zero())
    print("Stan qubitu po pomiarze 0: \n%s" % qubit.post_p_zero())

    print("P(1)= %s" % qubit.p_one())
    print("Stan qubitu po pomiarze 1: \n%s" % qubit.post_p_one())

    print("______________________\n")
    print("Obliczanie iloczynu tensorowego:\n")
    qubitA = Qubit(ComplexNumber(1, 0), ComplexNumber(0, 0))
    print("Qubit A:")
    print(qubitA)
    qubitB = Qubit(ComplexNumber(0, 0), ComplexNumber(1, 0))
    print("Qubit B:")
    print(qubitB)

    print("Tensor dot:")
    qubitA.printTensorProduct(qubitA.tensordot(qubitB))
