# -*- coding: utf-8 -*-
from complexnumber import ComplexNumber
from matrix import Matrix
from quantum import One_Bit, Zero_Bit
from numpy import tensordot, kron


class Qubit:
    def __init__(self, alpha, beta):
        self.alpha = alpha
        self.beta = beta
        self.matrixForm = Matrix(([self.alpha], [self.beta]))

    def p_zero(self):
        return self.alpha.module() * self.alpha.module()

    def p_one(self):
        return self.beta.module() * self.beta.module()

    def post_p_zero(self):
        return Qubit(self.alpha.divByScalar(self.alpha.module()), ComplexNumber(0, 0))

    def post_p_one(self):
        return Qubit(ComplexNumber(0, 0), self.beta.divByScalar(self.beta.module()))

    def tensordot(self, other):
        return kron(self.matrixForm.matrix, other.matrixForm.matrix)

    def printTensorProduct(self, tensor):
        for val in tensor:
            print("[" + str(val[0]) + "]")


    def __str__(self):
        ret = "Alpha: "
        ret += str(self.alpha) + " \n"
        ret += str(Zero_Bit) + "\nBeta: "
        ret += "+" if self.beta.Re >= 0 else ""
        ret += str(self.beta) + " \n"
        ret += str(One_Bit)
        return ret
